CREATE TABLE `merchants`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NULL,
  `code` varchar(50) NULL,
  `secret` varchar(50) NULL,
  `status` tinyint(1) NULL,
  `created_at` timestamp(0) NULL,
  `created_by` int(0) NULL,
  `extra` text NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `transactions`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `merchant_code` varchar(50) NOT NULL,
  `merchant_lat` varchar(20) NULL,
  `merchant_lng` varchar(20) NULL,
  `merchant_address` varchar(255) NULL,
  `merchant_district` varchar(255) NULL,
  `merchant_state` varchar(255) NULL,
  `customer_lat` varchar(20) NULL,
  `customer_lng` varchar(20) NULL,
  `customer_address` varchar(255) NULL,
  `customer_district` varchar(255) NULL,
  `customer_state` varchar(255) NULL,
  `requested_date` datetime(0) NULL,
  `created_date` datetime(0) NULL,
  `token` varchar(255) NULL,
  `distance` varchar(20) NULL,
  `distance_unit` varchar(10) NULL,
  `price` varchar(20) NULL,
  `price_unit` varchar(10) NULL,
  `time` varchar(20) NULL,
  `time_unit` varchar(10),
  PRIMARY KEY (`id`)
);

ALTER TABLE `transactions`
  ADD COLUMN `order_id` int(10) UNSIGNED NULL AFTER `time_unit`;

ALTER TABLE `user_master`
  MODIFY COLUMN `usertype` enum('','user','driver','merchant') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `fbid`;

ALTER TABLE `merchants`
  ADD COLUMN `user_id` int(0) UNSIGNED NOT NULL AFTER `extra`;