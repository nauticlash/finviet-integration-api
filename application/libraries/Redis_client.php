<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Redis_client
{
    /**
     * CodeIgniter instance.
     * refer to: https://github.com/phpredis/phpredis#installation
     * @var object
     */
    private $redis;
    private $newOrderChannel = 'new_order_notification';

    public function __construct()
    {
        //establish Redis connection
        $host = config_item('redis_host');
        $port = config_item('redis_port');
        $timeout = config_item('redis_timeout');
        $this->redis = new Redis();
        $this->redis->pconnect($host, $port, $timeout);
    }
    
    public function sendNewOrderNotification($orderData)
    {
        log_message('info', 'publish for key:' . $this->newOrderChannel . ', value:' . json_encode($orderData));
        return $this->redis->publish($this->newOrderChannel, json_encode($orderData));
    }

}
