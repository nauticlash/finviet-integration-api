<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class API_Controller extends CI_Controller {

    public $r_data = [];
    public $fields = [];
    public $required_fields = [];
    public $transactionData;
    public $merchantData;
    public $order_total = 0;

    function __construct() {
        parent::__construct();
    }

    function validate($request) {
        $this->load->model('merchants');
        $this->load->model('transactions');
        $concatString = '';
        foreach ($this->fields as $key) {
            $requestData = isset($request[$key]) ? trim($request[$key]) : '';
            if (in_array($key, $this->required_fields) && $requestData == '') {
                $this->r_data['status'] = 0;
                $this->r_data['result'] = [
                    'errorCode' => 6100,
                    'message' => 'The data for ' . $key . ' is required'
                ];
                $this->return_data($this->r_data);
            } else {
                switch ($key) {
                    case 'merchantCode':
                        $this->merchantData = $this->merchants->checkMerchant($requestData);
                        if (!$this->merchantData->secret) {
                            $this->r_data['result'] = [
                                'errorCode' => 6101,
                                'message' => 'Verify merchant failed!'
                            ];
                            $this->return_data($this->r_data);
                        }
                        break;
                    case 'transactionId':
                        $this->transactionData = $this->transactions->getById($requestData);
                        if (!$this->transactionData) {
                            $this->r_data['result'] = [
                                'errorCode' => 6103,
                                'message' => 'Transaction ID not exists in system'
                            ];
                            $this->return_data($this->r_data);
                        }
                        break;
                    case 'timestamp':
                        $requestData = $this->merchantData->secret . $requestData;
                        break;
                    case 'checksum':
                        $hash = hash('sha256', $concatString);
                        if ($hash != $requestData) {
                            $this->r_data['result'] = [
                                'errorCode' => 6000,
                                'message' => 'The input checksum is not valid',
                                'hash' => $hash,
//                                'concat' => $concatString
                            ];
                            $this->return_data($this->r_data);
                        }
                        break;
                    case 'orderItems':
                        $orderItems = json_decode($requestData);
                        if (!$orderItems) {
                            $this->r_data['result'] = [
                                'errorCode' => 6100,
                                'message' => 'The orderItems parameter is not in the required format',
                            ];
                            $this->return_data($this->r_data);
                        } else {
                            if (count($orderItems) > 0) {
                                $total = 0;
                                foreach ($orderItems as $item) {
                                    if (!property_exists($item, "name") || !property_exists($item, "quantity") || !property_exists($item, 'amount')) {
                                        $this->r_data['result'] = [
                                            'errorCode' => 6100,
                                            'message' => 'The orderItems parameter is not in the required format',
                                        ];
                                        $this->return_data($this->r_data);
                                    } else {
                                        $total += intval(($item->amount * $item->quantity));
                                    }
                                }
                                $this->order_total = intval($total);
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            $concatString .= $requestData;
        }
    }

    public function return_data($data) {
        header('Content-Type: application/json');
        log_message('debug', 'Return Data - ' . json_encode($data));
        echo json_encode($data);
        exit();
    }
}

