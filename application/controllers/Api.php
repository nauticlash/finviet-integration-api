<?php
require_once 'API_Controller.php';
require_once __DIR__ . '/../../vendor/firebase/php-jwt/src/JWT.php';

use \Firebase\JWT\JWT;

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends API_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->r_data['status'] = 0;
    }

    public function createTransaction()
    {
        $this->load->model('transactions');
//        $this->load->model('holiday_pricing');
        $this->required_fields = ['merchantCode', 'merchantLat', 'merchantLong', 'merchantAddress', 'merchantDistrict',
            'merchantState', 'customerLat', 'customerLong', 'customerAddress', 'customerDistrict', 'customerState',
            'timestamp', 'checksum'];
        $this->fields = ['merchantCode', 'merchantLat', 'merchantLong', 'merchantAddress', 'merchantDistrict',
            'merchantState', 'customerLat', 'customerLong', 'customerAddress', 'customerDistrict', 'customerState',
            'timestamp', 'checksum'];
        $request = $this->input->post();
        $this->validate($request);
        log_message('debug', 'createTransaction - ' . json_encode($request));
        $distance = 0;
        $distanceInKm = 0;
        $distanceUnit = 'm';
        $time = 0;
        $timeUnit = 'second';
        $price = 0;
        $priceUnit = '';
        $pricing = $this->transactions->getPricing();
        $heremapUrl = HEREMAPS_API . 'calculateroute.json?app_id=' . HEREMAPS_APP_ID . '&app_code=' . HEREMAPS_APP_CODE
            . '&waypoint0=geo!' . $request['customerLat'] . ',' . $request['customerLong'] . '&waypoint1=geo!' .
            $request['merchantLat'] . ',' . $request['merchantLong'] . '&routeattributes=sm&mode=fastest;bicycle';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $heremapUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $responseData = curl_exec($ch);
        $responseData = json_decode($responseData, true);
        curl_close($ch);
        if (!empty($responseData['response']) && !empty($responseData['response']['route'])
            && !empty($responseData['response']['route'][0]['summary'])) {
            $summary = $responseData['response']['route'][0]['summary'];
            $distance = $summary['distance'] ? $summary['distance'] : 0;
            $distanceInKm = round($distance / 1000, 2);
            $time = $summary['baseTime'] ? $summary['baseTime'] : 0;
            if ($pricing) {
                $price = $pricing->min_km_price_re;
                if ($distanceInKm > $pricing->min_km_re) {
                    $diffKm = ceil($distanceInKm - $pricing->min_km_re);
                    $price += $diffKm * $pricing->min_up_price_re;
                }
            }
        } else {
            $this->r_data['result'] = [
                'errorCode' => 7000,
                'message' => 'Route not found!'
            ];
            $this->return_data($this->r_data);
        }
        $key = "bungkusit";
        $tokenData = [
            'merchantCode' => $request['merchantCode'],
        ];
        $token = JWT::encode($tokenData, $key);
        $data = [
            'merchant_code' => $request['merchantCode'],
            'merchant_lat' => $request['merchantLat'],
            'merchant_lng' => $request['merchantLong'],
            'merchant_address' => $request['merchantAddress'],
            'merchant_district' => $request['merchantDistrict'],
            'merchant_state' => $request['merchantState'],
            'customer_lat' => $request['customerLat'],
            'customer_lng' => $request['customerLong'],
            'customer_address' => $request['customerAddress'],
            'customer_district' => $request['customerDistrict'],
            'customer_state' => $request['customerState'],
            'requested_date' => date('Y-m-d H:i:s', $request['timestamp']),
            'created_date' => date('Y-m-d H:i:s'),
            'token' => $token,
            'distance' => $distance,
            'distance_unit' => $distanceUnit,
            'price' => $price,
            'price_unit' => $priceUnit,
            'time' => $time,
            'time_unit' => $timeUnit
        ];
        $transactionId = $this->transactions->insert($data);
        $this->r_data['status'] = 1;
        $this->r_data['result'] = [
            'token' => $token,
            'transactionId' => $transactionId,
            'distance' => $distanceInKm . ' Km',
            'price' => intval($price),
            'currency' => 'VND'
        ];
        $this->return_data($this->r_data);
    }

    public function createOrder()
    {
        $this->load->model('transactions');
        $this->required_fields = ['merchantCode', 'merchantName', 'merchantContact', 'customerName', 'customerEmail', 'customerContact',
            'orderId', 'orderItems', 'transactionId', 'timestamp', 'token', 'checksum'];
        $this->fields = ['merchantCode', 'merchantName', 'merchantContact', 'customerName', 'customerEmail', 'customerContact',
            'customerContact2', 'orderId', 'orderItems', 'orderTotal', 'orderShippingFee', 'notes', 'extra', 'transactionId', 'timestamp', 'token',
            'checksum'];
        $request = $this->input->post();
        log_message('debug', 'createOrder - ' . json_encode($request));
        $this->validate($request);
        if (!empty($this->transactionData->order_id)) {
            $this->r_data['result'] = [
                'errorCode' => 6104,
                'message' => 'Duplicate Transaction ID'
            ];
            $this->return_data($this->r_data);
        }
        $orderinfo = $this->transactionData;
        $orderData = [
            "merchant_id" => $this->merchantData->id,
            "cash_payment" => "Pay at Drop Off",
            "courier_service_name" => "",
            "delivery_address" => $orderinfo->customer_address,
            "delivery_type" => "",
            "description" => !empty($request['notes']) ? $request['notes'] : '',
            "destination_address" => "",
            "dlat" => "",
            "dllat" => $orderinfo->customer_lat,
            "dllng" => $orderinfo->customer_lng,
            "dlng" => "",
            "images" => "",
            "is_rain" => 0,
            "lastorderid" => "",
            "no_of_km" => round($orderinfo->distance / 1000, 2),
            "ordertype" => "re",
            "pickup_address" => $orderinfo->merchant_address,
            "pickup_city" => $orderinfo->merchant_district,
            "pickup_country" => "",
            "pickup_postal" => "",
            "order_items" => $request['orderItems'],
            "order_id" => $request['orderId'],
            "order_total" => !empty($request['orderTotal']) ? $request['orderTotal'] : (!empty($this->order_total) ? $this->order_total : 0),
            "extra" => !empty($request['extra']) ? $request['extra'] : '',
            "merchant_contact" => $request['merchantContact'],
            "customer_contact" => $request['customerContact'],
            "customer_contact_2" => !empty($request['customerContact2']) ? $request['customerContact2'] : '',
            "pickup_state" => $orderinfo->merchant_state,
            "plat" => $orderinfo->merchant_lat,
            "platform" => "api",
            "plng" => $orderinfo->merchant_lng,
            "promoamount" => 0,
            "promocode" => "",
            "recipient_name" => $request['customerName'],
            "sender_name" => $request['merchantName'],
            "tipamt" => 0,
            "user_cash_payment" => $orderinfo->price,
            "userid" => $this->merchantData->user_id,
            "transaction_id" => $this->transactionData->id
        ];
        $requestData = [
            "service" => "placeOrder",
            "auth" => [
                "token" => $this->merchantData->code,
                "id" => $this->merchantData->user_id,
            ],
            "request" => ["data" => $orderData],
        ];
        $url = API_URL;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));

        $response = curl_exec($ch);
        curl_close($ch);
        log_message('debug', 'createOrder Response - ' . $response . ' - ' . json_encode($requestData));

        $response = json_decode($response);
        if (!empty($response->success)) {
            $this->load->library('redis_client');
            $orderId = $response->orderid;
            $this->transactions->updateOrder($this->transactionData->id, $orderId);
            $notificationData = [
                "token" => $this->merchantData->code,
                "user_id" => $this->merchantData->user_id,
                "order_id" => $orderId,
                "lat" => $orderinfo->merchant_lat,
                "lng" => $orderinfo->merchant_lng,
                "radius" => 15
            ];
            $this->redis_client->sendNewOrderNotification($notificationData);
            $this->r_data['status'] = 1;
            $this->return_data($this->r_data);
        } else {
            $this->r_data['status'] = 0;
            $this->r_data['result'] = [
                'errorCode' => 6107,
                'message' => "Create Order Failed!",
            ];
            $this->return_data($this->r_data);
        }
    }

    public function updateOrderStatus()
    {
        $this->required_fields = ['merchantCode', 'transactionId', 'status', 'timestamp', 'checksum'];
        $this->fields = ['merchantCode', 'transactionId', 'status', 'timestamp', 'checksum'];
        $validStatus = ['canceled', 'ready_pickup'];
        $request = $this->input->post();
        $this->validate($request);
        if (in_array($request['status'], $validStatus)) {
            if ($request['status'] == 'canceled') {
                $data = [
                    "service" => "cancel_order",
                    "auth" => [
                        "token" => $this->merchantData->code,
                        "id" => $this->merchantData->user_id,
                    ],
                    "request" => [
                        "orderid" => $this->transactionData->order_id,
                    ]
                ];
                $url = API_URL;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                $response = curl_exec($ch);
                curl_close($ch);

                $response = json_decode($response);
            }
            $this->r_data['status'] = 1;
            $this->return_data($this->r_data);
        } else {
            $this->r_data['result'] = [
                'errorCode' => 6100,
                'message' => 'The status parameter is not in the required format'
            ];
            $this->return_data($this->r_data);
        }
    }

    public function getOrderStatus()
    {
        $this->required_fields = ['merchantCode', 'transactionId', 'timestamp', 'checksum'];
        $this->fields = ['merchantCode', 'transactionId', 'timestamp', 'checksum'];
        $request = $this->input->post();
        $this->validate($request);
        $requestData = [
            "service" => "get_order_details",
            "auth" => [
                "token" => $this->merchantData->code,
                "id" => $this->merchantData->user_id,
            ],
            "request" => [
                "orderid" => $this->transactionData->order_id,
                "type" => "user"
            ],
        ];
        $url = API_URL;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));

        $response = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($response);
        if (!empty($response->success) && !empty($response->data)) {
            $result = $response->data;
            $this->r_data['status'] = 1;
            if ($result->rider_id) {
                $rider_name = !empty($result->firstname) ? $result->firstname : '';
                $riderLat = !empty($result->lat) ? $result->lat : 0;
                $riderLong = !empty($result->lng) ? $result->lng : 0;
                if ($result->status == 'Complete' && $result->riderstatus == 'Complete') {
                    $this->r_data['result'] = [
                        'orderId' => $this->transactionData->id,
                        'orderStatus' => 3,
                        'riderName' => $rider_name,
                        'riderLat' => $riderLat,
                        'riderLong' => $riderLong
                    ];
                } else if ($result->status == 'Canceled') {
                    if ($result->cancelby == $result->rider_id) {
                        $this->r_data['result'] = [
                            'orderId' => $this->transactionData->id,
                            'orderStatus' => 5,
                            'riderName' => $rider_name,
                            'riderLat' => $riderLat,
                            'riderLong' => $riderLong
                        ];
                    } else {
                        $this->r_data['result'] = [
                            'orderId' => $this->transactionData->id,
                            'orderStatus' => 6,
                            'riderName' => $rider_name,
                            'riderLat' => $riderLat,
                            'riderLong' => $riderLong
                        ];
                    }
                } else {
                    if ($result->is_pickup == 3) {
                        $this->r_data['result'] = [
                            'orderId' => $this->transactionData->id,
                            'orderStatus' => 2,
                            'riderName' => $rider_name,
                            'riderLat' => $riderLat,
                            'riderLong' => $riderLong
                        ];
                    } else {
                        $this->r_data['result'] = [
                            'orderId' => $this->transactionData->id,
                            'orderStatus' => 1,
                            'riderName' => $rider_name,
                            'riderLat' => $riderLat,
                            'riderLong' => $riderLong
                        ];
                    }
                }
            } else {
                if ($result->status == 'Canceled') {
                    $this->r_data['result'] = [
                        'orderId' => $this->transactionData->id,
                        'orderStatus' => 6,
                        'riderName' => '',
                        'riderLat' => 0,
                        'riderLong' => 0
                    ];
                } else {
                    $this->r_data['result'] = [
                        'orderId' => $this->transactionData->id,
                        'orderStatus' => 0,
                        'riderName' => '',
                        'riderLat' => 0,
                        'riderLong' => 0
                    ];
                }
            }
        } else {
            $this->r_data['result'] = [
                'errorCode' => 6105,
                'message' => 'Order ID not exists in system'
            ];
        }

        $this->return_data($this->r_data);
    }

}

