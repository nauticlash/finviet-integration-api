<?php
require('Common.php');

defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions extends Common {

    public $table = "transactions";

    public function getPricing() {
        $this->db->where('is_regular=', '1');
        return $this->db->get("holiday_pricing")->row();
    }

    public function updateOrder($transactionId, $orderId) {
        $this->db->where('id', $transactionId);
        return $this->db->update($this->table, array('order_id' => $orderId));
    }
}
