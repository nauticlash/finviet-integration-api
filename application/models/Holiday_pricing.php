<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class holiday_pricing extends common {

    public $table = "holiday_pricing";

    public function getPricing()
    {

        $this->db->where('is_regular=', '1');
        return $this->db->get($this->table)->row();
    }
}
