<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Merchants extends CI_Model {

    private $table = "merchants";

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function checkMerchant($code)
    {
        $query = "SELECT * FROM $this->table WHERE `code` = ? AND `status` = 1";
        $query = $this->db->query($query, $code);
        $result = $query->row();
        return $result;
    }
}
