<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Model {

    public $table = "";

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function getById($id) {
        $this->db->where('id', $id);
        $result = $this->db->get($this->table)->result();
        return !empty($result[0]) ? $result[0] : false;
    }
}